﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin_App
{
    class Account
    {
        private int checkedInTicket;
        private int checkedInCamp;

        public int CheckedInTicket
        {
            get { return checkedInTicket; }
            set { checkedInTicket = value; }
        }
        public int CheckedInCamp
        {
            get { return checkedInCamp; }
            set { checkedInCamp = value; }
        }
        public Account(int checkedInTicket, int checkedInCamp)
        {
            this.checkedInTicket = checkedInTicket;
            this.checkedInCamp = checkedInCamp;
        }
    }
}
