﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Phidget22;
using Phidget22.Events;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Security.Cryptography;
using System.IO;
using System.Globalization;
using System.Threading;
using System.Diagnostics;

namespace Admin_App
{
    public partial class Form1 : Form
    {
        int CheckedTicket, CheckedCamp;
        int NumberOfTickets, NotReturnedItems;
        double NewConsumablesPrice, currentPrice, currentDeposit, newDeposit, depoist1 = 0;
        MySqlConnection connection = new MySqlConnection("server=studmysql01.fhict.local;" +
                                                         "database=dbi380048;" +
                                                         "user id=dbi380048;" +
                                                         "password=pcsb;");
        FileStream fs;
        StreamReader sr;
        LogFile log;
        string filename = string.Empty;

        List<Account> CheckAccount = new List<Account>();
        public Form1()
        {
            InitializeComponent();
        }
        //This button allows user to pick a log file
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            var browser = new OpenFileDialog();

            browser.Filter = "Text files (*.txt)|*.txt";
            browser.RestoreDirectory = true;
            browser.InitialDirectory = filename;

            if (browser.ShowDialog() == DialogResult.OK)
            {
                filename = browser.FileName;
                lblFileName.Text = browser.FileName;
                //sr.FileName = browser.FileName;
                lblStatus.Text = "Transaction file selected, run transacions or choose another file";
                lbAtmTransactionLog.Items.Clear();
            }
        }
        //This Button sends logs to the database
        private void btnProcess_Click(object sender, EventArgs e)
        {           
            try
            {           
                log = new LogFile();
                fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
                sr = new StreamReader(fs);
                
                log.ProcessFile(fs, sr);
                //fs.Close();

                string query;
                MySqlCommand command;
                int tempint;
                string sqlStartDate = $"{log.StartTime.Year}-{log.StartTime.Month}-{log.StartTime.Day} {log.StartTime.Hour}:{log.StartTime.Minute}:{log.StartTime.Second}";
                string sqlEndDate = $"{log.EndTime.Year}-{log.EndTime.Month}-{log.EndTime.Day} {log.EndTime.Hour}:{log.EndTime.Minute}:{log.EndTime.Second}";

                for (int i = 0; i < log.NumberOfDeposits; i++)
                {
                    connection.Open();
                    query = $"INSERT INTO deposits (StartTime, EndTime, IBAN, Amount) " + $"VALUES ('" + sqlStartDate + "', '" + sqlEndDate + "', '" + log.Deposits[i].Item1 + "', '" + log.Deposits[i].Item2 + "')";
                    command = new MySqlCommand(query, connection);
                    tempint = command.ExecuteNonQuery();
                    connection.Close();
                }
                MessageBox.Show("Successfully exported the log file to the database!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information); 
            }        
            catch (Exception ex) when (!Debugger.IsAttached)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
            finally
            {
                fs.Close();
                connection.Close();
            }
        }
        //use to check the number of people that are inside the festival
        private void CheckForAll()
        {
            List<Account> temp = new List<Account>();
            string sql = "SELECT * FROM accounts";
            MySqlCommand command = new MySqlCommand(sql, connection);

            try
            {
                connection.Open();

                MySqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    CheckedTicket = Convert.ToInt32(reader["CheckedInEvent"]);

                    CheckedCamp = Convert.ToInt32(reader["CheckedInCamping"]);

                    temp.Add(new Account(CheckedTicket, CheckedCamp));
                }
                reader.Close();
                CheckAccount = temp;
            }
            catch
            {
                Console.WriteLine("Getting Account crashed");
            }
            finally
            {
                connection.Close();
            }
        }
        // check number of tickets sold
        private int CheckTicketNumber()
        {          
            string sql = "SELECT COUNT(*) FROM tickets WHERE Id > 0";
            MySqlCommand command = new MySqlCommand(sql, connection);
            try
            {
                connection.Open();
                NumberOfTickets = Convert.ToInt32(command.ExecuteScalar());
                
            }
            catch
            {
                Console.WriteLine("Getting Tickets crashed");
            }
            finally
            {
                connection.Close();
            }
            return NumberOfTickets;
        }
        //check number of loaned items that are not yet returned
        private int CheckUnreturnedLoanedItems()
        {
            string sql = "SELECT COUNT(*) FROM loans WHERE IsReturned = 0";
            MySqlCommand command = new MySqlCommand(sql, connection);
            try
            {
                connection.Open();
                NotReturnedItems = Convert.ToInt32(command.ExecuteScalar());
                return NotReturnedItems;
            }
            catch
            {
                Console.WriteLine("Getting UnReturnedItems crashed");
            }
            finally
            {
                connection.Close();
            }
            return NotReturnedItems;
        }
        //calculate income from foods and drinks
        private void CalculateConsumablesIncome()
        {
            double price = 0;
            string sql = "SELECT * FROM consumables";
            MySqlCommand command = new MySqlCommand(sql, connection);
            try
            {
                connection.Open();

                MySqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    NewConsumablesPrice = Convert.ToDouble(reader["Price"]);

                    price += NewConsumablesPrice;
                }
                reader.Close();
                currentPrice = price;
            }
            catch
            {
                Console.WriteLine("Getting UnReturnedItems crashed");
            }
            finally
            {
                connection.Close();
            }
            
        }

        //calculate all deposits made from logs
        private void CheckForDeposits()
        {
            double deposit = 0;
            string sql = "SELECT * FROM deposits";
            MySqlCommand command = new MySqlCommand(sql, connection);

            try
            {
                connection.Open();

                MySqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    newDeposit = Convert.ToDouble(reader["Amount"]);

                    deposit += newDeposit;
                }
                reader.Close();
                currentDeposit = deposit;
            }
            catch
            {
                Console.WriteLine("Getting Account crashed");
            }
            finally
            {
                connection.Close();
            }
        }
        private List<Account> CheckedInTicket()
        {
            List<Account> temp = new List<Account>();
            foreach (Account c in CheckAccount)
            {
                if (c.CheckedInTicket == 1)
                {
                    temp.Add(c);
                }
            }
            return temp;
        }
        private List<Account> CheckedOutTicket()
        {
            List<Account> temp = new List<Account>();
            foreach (Account c in CheckAccount)
            {
                if (c.CheckedInTicket == 0)
                {
                    temp.Add(c);
                }
            }
            return temp;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            CheckForAll();
            CheckForDeposits();
            this.chtCheckIn.Series["Entrance Gate Sign In"].Points.AddY(CheckedInTicket().Count);
            this.chtCheckIn.Series["Entrance Gate Sign Out"].Points.AddY(CheckedOutTicket().Count);
            this.chtCheckIn.Series["Camping Gate Sign In"].Points.AddY(CheckedInCamp().Count);
            this.chtCheckIn.Series["Camping Gate Sign Out"].Points.AddY(CheckedOutCamp().Count);

            this.chtDeposits.Series["Deposits"].Points.AddY(depoist1);
            this.chtDeposits.Series["Deposits"].Points.AddY(currentDeposit);

            depoist1 = currentDeposit;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            connection.Close();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            this.chtCheckIn.Update();
            this.chtDeposits.Update();
        }

        private void btnInfoUpdate_Click(object sender, EventArgs e)
        {
            CheckForAll();
            CheckForDeposits();
            CheckTicketNumber();
            CheckUnreturnedLoanedItems();
            CalculateConsumablesIncome();

            lblConsumablesSold.Text = "€ " + currentPrice.ToString();
            lblUnreturnedItems.Text = NotReturnedItems.ToString();
            lblNrTicketsSold.Text = NumberOfTickets.ToString(); 
            lblTotalDeposit.Text = "€ " + currentDeposit.ToString();
            lblPeopleAtFair.Text = CheckedInTicket().Count.ToString();
        }

        //This Button Shows the logs in the ListBox
        private void btnFillListBox_Click(object sender, EventArgs e)
        {
            log = new LogFile();
            fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
            sr = new StreamReader(fs);

            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                lbAtmTransactionLog.Items.Add(line);
            }
        }

        private List<Account> CheckedInCamp()
        {
            List<Account> temp = new List<Account>();
            foreach (Account c in CheckAccount)
            {
                if (c.CheckedInCamp == 1)
                {
                    temp.Add(c);
                }
            }
            return temp;
        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private List<Account> CheckedOutCamp()
        {
            List<Account> temp = new List<Account>();
            foreach (Account c in CheckAccount)
            {
                if (c.CheckedInCamp == 0)
                {
                    temp.Add(c);
                }
            }
            return temp;
        }
    }
}
