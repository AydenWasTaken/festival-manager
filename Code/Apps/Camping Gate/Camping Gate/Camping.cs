﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Camping_Gate
{
    class Camping
    {
        private List<Customer> Dummies = new List<Customer>();

        public bool AddPerson(Customer p)
        {
            if (this.SearchPerson(p.getId()) == null)
            {
                this.Dummies.Add(p);
                return true;
            }
            else
            {
                return false;
            }
        }
        public Customer SearchPerson(string id)
        {
            foreach (Customer p in this.Dummies)
            {
                if (p.getId() == id)
                { return p; }
            }
            return null;
        }
        public List<Customer> SearchPeople(string name)
        {
            List<Customer> temp = new List<Customer>();
            foreach (Customer p in this.Dummies)
            {
                if (p.Name == name)
                {
                    temp.Add(p);
                }
            }
            return temp;
        }
        public List<Customer> ShowAllPeople()
        {
            return this.Dummies;
        }
    }
}
