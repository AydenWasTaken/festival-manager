﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Camping_Gate
{
    class Customer
    {
        private string accountId;
        private string idCard;
        private string name;
        private string ticket;

        public string AccountId
        {
            get { return accountId; }
            set { accountId = value; }
        }
        public string IdCard
        {
            get { return idCard; }
            set { idCard = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string Ticket
        {
            get { return ticket; }
            set { ticket = value; }
        }
        public Customer(string accountID, string name, string id)
        {
            this.accountId = accountID;
            this.name = name;
            this.idCard = id;
        }
        public Customer(string accountID, string name, string id, string ticket)
        {
            this.accountId = accountID;
            this.name = name;
            this.idCard = id;
            this.ticket = ticket;
        }

        public string getId()
        {
            return this.IdCard;
        }
        public override String ToString()
        {
            String holder;
            holder = "Account ID: " + accountId + "Id Card: " + this.IdCard + ", Name: " + this.Name;
            return holder;
        }
    }
}
