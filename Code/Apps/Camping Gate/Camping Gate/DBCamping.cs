﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Phidget22;
using Phidget22.Events;
using MySql.Data.MySqlClient;
using MySql.Data;

namespace Camping_Gate
{
    class DBCamping
    {

        string name, account, rfid, ticket; int camp;
        public MySqlConnection connection;
        WithCamping camper;


        public DBCamping()
        {
            string connInfo = "server=studmysql01.fhict.local;" +
                                "database=dbi380048;" +
                                "user id=dbi380048;" +
                                "password=pcsb;";
            connection = new MySqlConnection(connInfo);
        }
        public List<Customer> GetAllPeople()
        {
            List<Customer> tempList = new List<Customer>();

            string sql = "SELECT * FROM accounts";
            MySqlCommand command = new MySqlCommand(sql, connection);
            //Console.WriteLine("Set Command 1");

            try
            {
                connection.Open();
                //Console.WriteLine("Connetion Open");

                MySqlDataReader reader = command.ExecuteReader();
                //Console.WriteLine("Reader Open");

                while (reader.Read())
                {
                    rfid = Convert.ToString(reader["RFID"]);
                    //Console.WriteLine("Got RFID: " + rfid);

                    name = Convert.ToString(reader["Name"]);
                    //Console.WriteLine("Got name: " + name);

                    account = Convert.ToString(reader["Email"]);
                    //Console.WriteLine("Got Email: " + account);

                    ticket = Convert.ToString(reader["TicketId"]);
                    //Console.WriteLine("Got TicketId: " + ticket);

                    tempList.Add(new Customer(account, name, rfid, ticket));

                    //Console.WriteLine("Customer added");
                }

                reader.Close();
            }
            catch
            {
                Console.WriteLine("Getting People crashed");
            }
            finally
            {
                connection.Close();
            }

            return tempList;
        }
        public WithCamping GetCamping(Customer c)
        {
            string sql2 = "Select * FROM tickets WHERE Id = '" + c.Ticket + "'";
            MySqlCommand command2 = new MySqlCommand(sql2, connection);
            //Console.WriteLine("Set command 2");


            try
            {
                connection.Open();
                //Console.WriteLine("Connetion Open");

                MySqlDataReader reader = command2.ExecuteReader();
                //Console.WriteLine("Reader2 Open");

                while (reader.Read())
                {
                    camp = Convert.ToInt32(reader["CampingId"]);
                    //Console.WriteLine("Got CampingId: " + camp);

                    camper = new WithCamping(account, name, rfid, camp);
                    //Console.WriteLine("Camper found");
                }
                reader.Close();
            }
            catch
            {
                Console.WriteLine("Getting camping crashed");
            }
            finally
            {
                connection.Close();
            }
            return camper;
        }

        private Customer SearchPerson(string id)
        {
            List<Customer> allPeople = GetAllPeople();

            foreach (Customer p in allPeople)
            {
                if (p.getId() == id)
                { return p; }
            }
            return null;
        }

        public void UpdateAccount(string id)
        {
            String sql = $"Update accounts SET CheckedInCamping = '1' Where RFID = '" + id + "';";
            MySqlCommand command = new MySqlCommand(sql, connection);
            try
            {
                if (id == null)
                {
                    MessageBox.Show("Person doesn't exist");
                }
                else
                {
                    connection.Open();
                    command.ExecuteNonQuery();

                    Console.WriteLine("Account Updated");
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Update account failed");
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
