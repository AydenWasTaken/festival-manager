﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Camping_Gate
{
    class WithCamping : Customer
    {
        private int campingNo;

        public int CampingNo
        {
            get { return campingNo; }
            set { campingNo = value; }
        }
        
        public WithCamping(string accountID, string name, string id, int campNo)
            : base(accountID, name, id)
        {
            this.campingNo = campNo;
        }
    }
}
