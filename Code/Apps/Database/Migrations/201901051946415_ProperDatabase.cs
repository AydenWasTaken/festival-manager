namespace Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProperDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        Email = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        Name = c.String(unicode: false),
                        Password = c.String(unicode: false),
                        Gender = c.Int(nullable: false),
                        Phone = c.String(unicode: false),
                        CheckedInEvent = c.Boolean(nullable: false),
                        CheckedInCamping = c.Boolean(nullable: false),
                        Address = c.String(unicode: false),
                        DateOfBirth = c.DateTime(nullable: false, precision: 0),
                        RFID = c.String(unicode: false),
                        TicketId = c.Int(),
                        PaidForTicket = c.Boolean(nullable: false),
                        CreditStored = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Email)
                .ForeignKey("dbo.Tickets", t => t.TicketId)
                .Index(t => t.TicketId);
            
            CreateTable(
                "dbo.Deposits",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AccountEmail = c.String(maxLength: 128, storeType: "nvarchar"),
                        Time = c.DateTime(nullable: false, precision: 0),
                        Amount = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Accounts", t => t.AccountEmail)
                .Index(t => t.AccountEmail);
            
            CreateTable(
                "dbo.Loans",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ItemId = c.Int(nullable: false),
                        AccountEmail = c.String(maxLength: 128, storeType: "nvarchar"),
                        IsReturned = c.Boolean(nullable: false),
                        From = c.DateTime(nullable: false, precision: 0),
                        To = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Accounts", t => t.AccountEmail)
                .ForeignKey("dbo.Items", t => t.ItemId, cascadeDelete: true)
                .Index(t => t.ItemId)
                .Index(t => t.AccountEmail);
            
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(unicode: false),
                        Quantity = c.Int(nullable: false),
                        PricePerHour = c.Double(nullable: false),
                        Time = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Tickets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DateOfPurchase = c.DateTime(nullable: false, precision: 0),
                        CampingId = c.Int(),
                        AmountOfPeople = c.Int(nullable: false),
                        Price = c.Double(nullable: false),
                        OtherEmails = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Campings", t => t.CampingId)
                .Index(t => t.CampingId);
            
            CreateTable(
                "dbo.Campings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsAvailable = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Consumables",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(unicode: false),
                        Quantity = c.Int(nullable: false),
                        TimesBought = c.Int(nullable: false),
                        Price = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Accounts", "TicketId", "dbo.Tickets");
            DropForeignKey("dbo.Tickets", "CampingId", "dbo.Campings");
            DropForeignKey("dbo.Loans", "ItemId", "dbo.Items");
            DropForeignKey("dbo.Loans", "AccountEmail", "dbo.Accounts");
            DropForeignKey("dbo.Deposits", "AccountEmail", "dbo.Accounts");
            DropIndex("dbo.Tickets", new[] { "CampingId" });
            DropIndex("dbo.Loans", new[] { "AccountEmail" });
            DropIndex("dbo.Loans", new[] { "ItemId" });
            DropIndex("dbo.Deposits", new[] { "AccountEmail" });
            DropIndex("dbo.Accounts", new[] { "TicketId" });
            DropTable("dbo.Consumables");
            DropTable("dbo.Campings");
            DropTable("dbo.Tickets");
            DropTable("dbo.Items");
            DropTable("dbo.Loans");
            DropTable("dbo.Deposits");
            DropTable("dbo.Accounts");
        }
    }
}
