namespace Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewData : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Deposits", "IBAN", c => c.String(unicode: false));
            AddColumn("dbo.Deposits", "StartTime", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.Deposits", "EndTime", c => c.DateTime(nullable: false, precision: 0));
            DropColumn("dbo.Deposits", "Time");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Deposits", "Time", c => c.DateTime(nullable: false, precision: 0));
            DropColumn("dbo.Deposits", "EndTime");
            DropColumn("dbo.Deposits", "StartTime");
            DropColumn("dbo.Deposits", "IBAN");
        }
    }
}
