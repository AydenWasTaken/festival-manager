﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Model
{
    public class Account
    {
        public string Name { get; set; }
        public string Password { get; set; }
        [Key]
        public string Email { get; set; }
        public Gender Gender { get; set; }
        public string Phone { get; set; }
        public bool CheckedInEvent { get; set; }
        public bool CheckedInCamping { get; set; }
        public string Address { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string RFID { get; set; }
        public List<Deposit> Deposits { get; set; }
        public List<Loan> Loans { get; set; }
        public int? TicketId { get; set; }
        public Ticket Ticket { get; set; }
        public bool PaidForTicket { get; set; }
        public double CreditStored { get; set; }
    }
}
