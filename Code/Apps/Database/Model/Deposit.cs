﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Model
{
    public class Deposit
    {
        public int Id { get; set; }
        public string AccountEmail { get; set; }
        [ForeignKey("AccountEmail")]
        public Account Account { get; set; }
        public string IBAN { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public double Amount { get; set; }
    }
}
