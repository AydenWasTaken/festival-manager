﻿using Database.Model;
using MySql.Data.Entity;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class RockfairContext : DbContext
    {
        // To apply changes to the db use Package Manager Console and type add-migration <migration name>
        // Then run update-database
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Camping> Campings { get; set; }
        public DbSet<Consumable> Consumables { get; set; }
        public DbSet<Deposit> Deposits { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Loan> Loans { get; set; }
    }
}
