﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfoDesk
{
    class CustomerCampInfo : CustomerInfo
    {
        private int campingNo;

        public int CampingNo
        {
            get { return campingNo; }
            set { campingNo = value; }
        }
        public CustomerCampInfo(string accountID, string name, string id, int campNo)
            : base(accountID, name, id)
        {
            this.campingNo = campNo;
        }
        public CustomerCampInfo(string accountID, string name, string id,string ticket , int campNo)
            : base(accountID, name, id, ticket)
        {
            this.campingNo = campNo;
        }
    }
}
