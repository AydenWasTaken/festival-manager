﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfoDesk
{
    class CustomerInfo
    {
        private string accountId;
        private string idCard;
        private string name;
        private string ticketNo;

        private int credit;

        public string AccountId
        {
            get { return accountId; }
            set { accountId = value; }
        }
        public string IdCard
        {
            get { return idCard; }
            set { idCard = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string TicketNo
        {
            get { return ticketNo; }
            set { ticketNo = value; }
        }
        public int Credit
        {
            get { return credit; }
            set { credit = value; }
        }

        public CustomerInfo(string accountID, string name, string id)
        {
            this.accountId = accountID;
            this.name = name;
            this.idCard = id;
            this.TicketNo = "No Ticket";
        }
        public CustomerInfo(string accountID, string name, string id, string ticketNo)
        {
            this.accountId = accountID;
            this.name = name;
            this.idCard = id;
            this.TicketNo = ticketNo;
        }
        public CustomerInfo(string accountID, string name, string id, string ticketNo,int Credit)
        {
            this.accountId = accountID;
            this.name = name;
            this.idCard = id;
            this.TicketNo = ticketNo;
            this.credit = Credit;
        }


        public CustomerInfo(string ticket)
        {
            this.ticketNo = ticket;
        }

        public string getId()
        {
            return this.IdCard;
        }
        public override String ToString()
        {
            String holder;
            holder = "Account ID: " + accountId + "Id Card: " + this.IdCard + ", Name: " + this.Name;
            return holder;
        }
    }
}
