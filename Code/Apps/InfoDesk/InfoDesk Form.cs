﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Phidget22;
using Phidget22.Events;
using MySql.Data.MySqlClient;
using MySql.Data;

namespace InfoDesk
{
    public partial class Form1 : Form
    {
        RFID rfid;
        DBInfoDesk dID;
        Random rnd;
        int NextTicket = 1;
        string CurrentTicket = "";
        string CurrentCamp = "";
        string id = "";
        int amountOfTickets = 0;
        bool GettingCamping = false;
        int credit = 0;

        public Form1()
        {
            InitializeComponent();
            dID = new DBInfoDesk();
            rnd = new Random();
            StartUp();
        }
        public void StartUp()
        {
            try
            {
                rfid = new RFID();

                rfid.Attach += rfid_Attach;
                rfid.Detach += rfid_Detach;
                rfid.Error += rfid_Error;
                rfid.Tag += rfid_Tag;
                rfid.TagLost += rfid_TagLost;

                rfid.Open();
            }
            catch (PhidgetException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
            catch
            {
                MessageBox.Show("something else");
            }
        }

        private void rfid_Attach(object sender, AttachEventArgs e)
        {
            panel2.BackColor = Color.LightCyan;
        }

        private void rfid_Detach(object sender, DetachEventArgs e)
        {
            panel2.BackColor = Color.Red;
        }

        private void rfid_Error(object sender, ErrorEventArgs e)
        {
            MessageBox.Show("error");
        }

        private void rfid_Tag(object sender, RFIDTagEventArgs e)
        {
            try
            {
                if (SearchPerson(e.Tag) == null)
                {
                    MessageBox.Show("Account\nUnregistered");
                }
                else
                {
                    Console.WriteLine("ticket: " + SearchPerson(e.Tag).TicketNo);
                    tbAccountName.Text = SearchPerson(e.Tag).Name;
                    tbAccountRFID.Text = SearchPerson(e.Tag).IdCard;

                    credit = SearchPerson(e.Tag).Credit;
                    Console.WriteLine("credit: "+ credit);

                    id = SearchPerson(e.Tag).IdCard;

                    if (SearchPerson(e.Tag).TicketNo == "-1" || SearchPerson(e.Tag).TicketNo == null || SearchPerson(e.Tag).TicketNo == "")
                    {
                        CurrentTicket = "No Ticket";
                        tbAssignTicket.Text = CurrentTicket;

                        CurrentCamp = "No Camping";
                        tbCampSpot.Text = CurrentCamp;
                    }
                    else
                    {
                        CurrentTicket = SearchPerson(e.Tag).TicketNo;
                        tbAssignTicket.Text = CurrentTicket;

                        CustomerCampInfo test = dID.GetCamping(SearchPerson(e.Tag));
                        if (test.CampingNo > 0)
                        {
                            CurrentCamp = test.CampingNo.ToString();
                            tbCampSpot.Text = CurrentCamp;
                        }
                        else
                        {
                            CurrentCamp = "No Camping";
                            tbCampSpot.Text = CurrentCamp;
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
        }
        private CustomerInfo SearchPerson(string id)
        {
            List<CustomerInfo> allPeople = dID.GetAllPeople();

            foreach (CustomerInfo p in allPeople)
            {
                if (p.getId() == id)
                { return p; }
            }
            return null;
        }
        private void SearchAvailables()
        {
            List<CampingInfo> allAvailableCamps = dID.GetAllCamping();
            cbCampSpot.Items.Clear();
            foreach (CampingInfo c in allAvailableCamps)
            {
                if (c.Available == 0)
                {
                    cbCampSpot.Items.Add(c.CampingNo);
                    Console.WriteLine(c.CampingNo + " spot added");
                }
            }
        }
        private string TicketGenerator()
        {
            List<CustomerInfo> allAvailableCamps = dID.GetAllTickets();

            List<int> temp = new List<int>();
            foreach (CustomerInfo c in allAvailableCamps)
            {
                temp.Add(Convert.ToInt32(c.TicketNo));
            }
            if (temp.Contains(NextTicket))
            {
                NextTicket++;
                TicketGenerator();
            }
            return NextTicket.ToString();
        }

        private void rfid_TagLost(object sender, RFIDTagLostEventArgs e)
        {

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            dID.connection.Close();
            rfid.Close();
        }


        private void btnCancel_Click(object sender, EventArgs e)
        {
            tbAccountRFID.Clear();
            tbAccountName.Clear();
            id = "";
            CurrentTicket = "";
            CurrentCamp = "";
            rbCampNo.Checked = true;
            rbTicketNo.Checked = true;
            amountOfTickets = 0;
            GettingCamping = false;
            credit = 0;
        }

        private void rbTicketYes_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTicketYes.Checked == true)
            {
                Console.WriteLine("CurrentTicket: " + CurrentTicket);
                if (CurrentTicket == "No Ticket")
                {
                    tbAssignTicket.Text = TicketGenerator();
                    amountOfTickets = Convert.ToInt32(cbNumberPeople.Text);
                    lblTotalPrice.Text = "€ " + getTotalPrice(GettingCamping, amountOfTickets).ToString();
                }
                else if (CurrentTicket == "")
                {
                    MessageBox.Show("No Registered Account");
                }
                else
                {
                    tbAssignTicket.Text = CurrentTicket;
                    MessageBox.Show("Already has ticket: " + CurrentTicket);

                    amountOfTickets = Convert.ToInt32(cbNumberPeople.Text);
                    MessageBox.Show("Can assign for more People");
                    lblTotalPrice.Text = "€ " + getTotalPrice(GettingCamping, amountOfTickets).ToString();
                }
            }
        }
        private void rbTicketNo_CheckedChanged(object sender, EventArgs e)
        {
            tbAssignTicket.Text = CurrentTicket;
            //tbNumberPeople.Text = "0";
            amountOfTickets = 0;
            lblTotalPrice.Text = "€ " + getTotalPrice(GettingCamping, amountOfTickets).ToString();
        }
        private void rbCampYes_CheckedChanged(object sender, EventArgs e)
        {

            if (rbCampYes.Checked == true)
            {
                Console.WriteLine("Current Camp: " + CurrentCamp);
                if (CurrentCamp == "No Camping")
                {
                    cbCampSpot.Enabled = true;
                    tbCampSpot.Text = "Select a Camp Spot";
                    SearchAvailables();
                    cbCampSpot.Text = cbCampSpot.SelectionStart.ToString();
                    GettingCamping = true;
                    lblTotalPrice.Text = "€ " + getTotalPrice(GettingCamping, amountOfTickets).ToString();
                }
                else if (CurrentCamp == "")
                {
                    MessageBox.Show("No Registered Account");
                }
                else
                {
                    tbCampSpot.Text = CurrentCamp;
                    MessageBox.Show("Already has Camp Spot: " + CurrentCamp);
                }
            }
        }

        private void rbCampNo_CheckedChanged(object sender, EventArgs e)
        {
            tbCampSpot.Text = CurrentCamp;
            cbCampSpot.Enabled = false;
            GettingCamping = false;
            lblTotalPrice.Text = "€ " + getTotalPrice(GettingCamping, amountOfTickets).ToString();
        }

        private void btnPay_Click(object sender, EventArgs e)
        {
            int campspot, ticket, People;

            if (CurrentTicket != "No Ticket")
            {
                //Has a Ticket and Camp Spot
                if (CurrentCamp != "No Camping")
                {
                    ticket = Convert.ToInt32(CurrentTicket);
                    Console.WriteLine(CurrentTicket);
                    campspot = Convert.ToInt32(CurrentCamp);
                    Console.WriteLine(campspot);
                    People = Convert.ToInt32(cbNumberPeople.Text);
                    Console.WriteLine(People);

                    if (SearchCredit(id) > getTotalPrice(GettingCamping, amountOfTickets))
                    {
                        dID.UpdateTicket(ticket, campspot, People, getTotalPrice(GettingCamping, amountOfTickets));
                        dID.UpdateAccount(id, ticket.ToString());
                        dID.UpdateCredit(id, SearchCredit(id) - getTotalPrice(GettingCamping, amountOfTickets));
                    }
                    else
                    {
                        MessageBox.Show("Not Enough Credits");
                    }

                    MessageBox.Show("Account already has Ticket and Camping");
                }
                else
                {
                    //Has a Ticket and Doesn't Want a Camp Spot(Cancelling Camping)
                    if (rbCampYes.Checked == false)
                    {
                        ticket = Convert.ToInt32(CurrentTicket);
                        Console.WriteLine(CurrentTicket);
                        campspot = -1;
                        People = Convert.ToInt32(cbNumberPeople.Text);
                        Console.WriteLine(People);

                        dID.UpdateAccount(id, ticket.ToString());
                        dID.UpdateTicket(ticket, campspot, People, getTotalPrice(GettingCamping, amountOfTickets));
                    }
                    //Has Ticket and Wants a Camp Spot
                    else if (rbCampYes.Checked == true)
                    {
                        if (Convert.ToInt32(cbCampSpot.Text) < 1)
                        {
                            MessageBox.Show("Select Camp Spot");
                        }
                        else
                        {
                            ticket = Convert.ToInt32(CurrentTicket);
                            Console.WriteLine(CurrentTicket);
                            campspot = Convert.ToInt32(cbCampSpot.Text);
                            Console.WriteLine(campspot);
                            People = Convert.ToInt32(cbNumberPeople.Text);
                            Console.WriteLine(People);

                            if (SearchCredit(id) > getTotalPrice(GettingCamping, amountOfTickets))
                            {
                                dID.UpdateTicket(ticket, campspot, People, getTotalPrice(GettingCamping, amountOfTickets));
                                dID.UpdateAccount(id, ticket.ToString());
                                dID.UpdateCredit(id, SearchCredit(id) - getTotalPrice(GettingCamping, amountOfTickets));
                                MessageBox.Show("CampSpot Added");
                            }
                            else
                            {
                                MessageBox.Show("Not Enough Credits");
                            }
                        }
                    }
                }
            }
            else
            {
                //Doesn't Have Ticket but wants a Camp Spot
                if (rbCampYes.Checked == true && rbTicketNo.Checked == true)
                {
                    MessageBox.Show("Purchuse a Tickect to get Camping Spot");
                }
                //Does't have Camping or a Ticket
                else if (rbCampNo.Checked == true && rbTicketNo.Checked == true)
                {
                    MessageBox.Show("Customer is not purchasing anything");
                }
                else
                {
                    //Wants only a Ticket
                    if (rbCampYes.Checked == false && rbTicketYes.Checked == true)
                    {
                        ticket = Convert.ToInt32(tbAssignTicket.Text);
                        Console.WriteLine(ticket);
                        campspot = -1;
                        People = Convert.ToInt32(cbNumberPeople.Text);
                        Console.WriteLine(People);

                        if (SearchCredit(id) > getTotalPrice(GettingCamping, amountOfTickets))
                        {
                            dID.MakeTicket(ticket, campspot, People, getTotalPrice(GettingCamping, amountOfTickets));
                            dID.UpdateAccount(id, ticket.ToString());
                            dID.UpdateCredit(id, SearchCredit(id) - getTotalPrice(GettingCamping, amountOfTickets));
                        }
                        else
                        {
                            MessageBox.Show("Not Enough Credits");
                        }
                    }
                    //Wants both a Ticket and a Camp Spot
                    else if (rbCampYes.Checked == true && rbTicketYes.Checked == true)
                    {
                        if (Convert.ToInt32(cbCampSpot.Text) < 1)
                        {
                            MessageBox.Show("Select Camp Spot");
                        }
                        else
                        {
                            ticket = Convert.ToInt32(tbAssignTicket.Text);
                            Console.WriteLine(ticket);
                            campspot = Convert.ToInt32(cbCampSpot.Text);
                            Console.WriteLine(campspot);
                            People = Convert.ToInt32(cbNumberPeople.Text);
                            Console.WriteLine(People);

                            if (SearchCredit(id) > getTotalPrice(GettingCamping, amountOfTickets))
                            {
                                dID.MakeTicket(ticket, campspot, People, getTotalPrice(GettingCamping, amountOfTickets));
                                dID.UpdateAccount(id, ticket.ToString());
                                dID.UpdateCredit(id, SearchCredit(id) - getTotalPrice(GettingCamping, amountOfTickets));
                                MessageBox.Show("CampSpot Added");
                            }
                            else
                            {
                                MessageBox.Show("Not Enough Credits");
                            }
                        }
                    }
                }
            }

            tbAccountRFID.Clear();
            tbAccountName.Clear();
            id = "";
            CurrentTicket = "";
            CurrentCamp = "";
            rbCampNo.Checked = true;
            rbTicketNo.Checked = true;
            amountOfTickets = 0;
            GettingCamping = false;
            credit = 0;

            lblTotalPrice.Text = "€ " + getTotalPrice(GettingCamping, amountOfTickets).ToString();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            rfid.Close();
            dID.connection.Close();
        }


        public double getTotalPrice(bool isCamping, int amountofTickets)
        {
            double price = 0;
            if (isCamping == true)
            {
                switch (amountofTickets)
                {
                    case 0:
                        {
                            price = 0;
                            break;
                        }
                    case 1:
                        {
                            price = (55) + 20 + (10);
                            break;
                        }
                    case 2:
                        {
                            price = (55 * 2) + 20 + (10 * 2);
                            break;
                        }
                    case 3:
                        {
                            price = (55 * 3) + 20 + (10 * 3);
                            break;
                        }
                    case 4:
                        {
                            price = (55 * 4) + 20 + (10 * 4);
                            price = (price / 100 * 95);
                            break;
                        }
                    case 5:
                        {
                            price = (55 * 5) + 20 + (10 * 5);
                            price = (price / 100 * 90);
                            break;
                        }
                    case 6:
                        {
                            price = (55 * 6) + 20 + (10 * 6);
                            price = (price / 100 * 85);
                            break;
                        }
                }
            }
            else if (isCamping == false)
            {
                switch (amountofTickets)
                {
                    case 0:
                        {
                            price = 0;
                            break;
                        }
                    case 1:
                        {
                            price = 55;
                            break;
                        }
                    case 2:
                        {
                            price = 55 * 2;
                            break;
                        }
                    case 3:
                        {
                            price = 55 * 3;
                            break;
                        }
                    case 4:
                        {
                            price = 55 * 4;
                            price = (price / 100) * 95;
                            break;
                        }
                    case 5:
                        {
                            price = 55 * 5;
                            price = (price / 100) * 90;
                            break;
                        }
                    case 6:
                        {
                            price = 55 * 6;
                            price = (price / 100) * 85;
                            break;
                        }
                }
            }
            price = Math.Round(price, 2);
            return price;
        }

        private void cbNumberPeople_SelectedIndexChanged(object sender, EventArgs e)
        {
            amountOfTickets = Convert.ToInt32(cbNumberPeople.Text);
            lblTotalPrice.Text = "€ " + getTotalPrice(GettingCamping, amountOfTickets).ToString();
        }

        private int SearchCredit(string id)
        {
            List<CustomerInfo> allPeople = dID.GetAllPeople();

            foreach (CustomerInfo p in allPeople)
            {
                if (p.getId() == id)
                {
                    return p.Credit;
                }
            }
            return 0;
        }
    }
}
