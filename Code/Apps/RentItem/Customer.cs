﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentItem
{
    class Customer
    {
        public string Email { get; set; }
        public string AccountId { get; set; }

        public Customer(string email, string accountid)
        {
            this.Email = email;
            this.AccountId = accountid;
        }

        public string GetAccountId()
        {
            return this.AccountId;
        }
    }
}
