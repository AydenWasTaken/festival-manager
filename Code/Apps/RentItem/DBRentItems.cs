﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using MySql.Data;

namespace RentItem
{
    class DBRentItems
    {
        public MySqlConnection connection;

        public DBRentItems()
        {
            string connInfo = "server=studmysql01.fhict.local;" +
                    "database=dbi380048;" +
                    "user id=dbi380048;" +
                    "password=pcsb;";
            connection = new MySqlConnection(connInfo);
        }

        public List<Customer> GetAccountsInfo()
        {
            List<Customer> temp = new List<Customer>();
            string sql = "SELECT * FROM accounts";
            MySqlCommand command = new MySqlCommand(sql, connection);
            try
            {
                connection.Open();
                Console.WriteLine("Got Connection");
                MySqlDataReader reader = command.ExecuteReader();
                Console.WriteLine("Started reader");
                string accountemail;
                string accountid;
                while (reader.Read())
                {
                    Console.WriteLine("reader started");
                    accountemail = Convert.ToString(reader["Email"]);
                    Console.WriteLine("Got Email");
                    accountid = Convert.ToString(reader["RFID"]);
                    Console.WriteLine("Got RFID");
                    temp.Add(new Customer(accountemail, accountid));
                }
            }
            catch
            {
                MessageBox.Show("Error while loading...");
            }
            finally
            {
                connection.Close();
            }
            return temp;
        }

        public List<Items> GetItemsInfo(ComboBox combo)
        {
            List<Items> temp = new List<Items>();
            string sql = "SELECT * FROM items";
            MySqlCommand command = new MySqlCommand(sql, connection);
            try
            {
                connection.Open();
                Console.WriteLine("Got Connection");
                MySqlDataReader reader = command.ExecuteReader();
                Console.WriteLine("Started reader");
                int itemid;
                string itemname;
                double itemprice;
                while (reader.Read())
                {
                    Console.WriteLine("reader started");
                    itemid = Convert.ToInt32(reader["Id"]);
                    Console.WriteLine("Got Id");
                    itemname = Convert.ToString(reader["Name"]);
                    Console.WriteLine("Got Name");
                    itemprice = Convert.ToDouble(reader["PricePerHour"]);
                    Console.WriteLine("Got Price");
                    temp.Add(new Items(itemid, itemname, itemprice));
                    combo.Items.Add(reader[0] + "." + reader[1] + ":" + reader[3] + " euros");
                }
            }
            catch
            {
                MessageBox.Show("Error while loading...");
            }
            finally
            {
                connection.Close();
            }
            return temp;
        }

        public void RentItem(ListBox rented_list, string email)
        {
            //string sql = "INSERT INTO loans (ItemId,From)" +
            //    "VALUES ('" + id + "','" + DateTime.Now.ToString("yyyy'-'MM'-'dd' 'HH':'mm':'ss") + "')'";
            //MySqlCommand command = new MySqlCommand(sql, connection);

            //string sql = "SELECT * FROM items";
            //MySqlCommand command = new MySqlCommand(sql, connection);
            //connection.Open();
            //MySqlDataReader reader = command.ExecuteReader();
            //while (reader.Read())
            //{

            try
            {
                foreach (string i in rented_list.Items)
                {
                    string item = i.ToString();
                    int id = Convert.ToInt32(item.ToString().Substring(0, 1));
                    string queryString = $"INSERT INTO `loans` (`ItemId`,`AccountEmail`,`From`)" +
                   $"VALUES ('" + id + "','" + email + "','" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + "')";
                    MySqlCommand command = new MySqlCommand(queryString, connection);
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
            }
            catch (MySql.Data.MySqlClient.MySqlException)
            {
                MessageBox.Show("Sorry,you can not rent items without rfid card...");
            }
        }

        public void QuantityDecrease(ListBox rented_list)
        {
            try
            {
                foreach (string i in rented_list.Items)
                {
                    string item = i.ToString();
                    int id = Convert.ToInt32(item.ToString().Substring(0, 1));
                    if (this.GetQuantiy(rented_list) > 0)
                    {
                        string queryString = "UPDATE items SET Quantity = Quantity-1 WHERE Id =" + id;
                        MySqlCommand command = new MySqlCommand(queryString, connection);
                        command.Connection.Open();
                        command.ExecuteNonQuery();
                        command.Connection.Close();
                    }
                }
            }
            catch (MySql.Data.MySqlClient.MySqlException)
            {
                MessageBox.Show("Error while loading...");
            }
        }

        public int GetQuantiy(ListBox rented_list)
        {
            int nr = 0;
            foreach (string i in rented_list.Items)
            {
                string item = i.ToString();
                int id = Convert.ToInt32(item.ToString().Substring(0, 1));
                string sql = "SELECT Quantity FROM items WHERE Id =" + id;
                MySqlDataReader reader = null;
                MySqlCommand command = new MySqlCommand(sql, connection);
                connection.Open();
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    nr = reader.GetInt32(0);
                }
                connection.Close();
            }
            return nr;
        }

        public void CreditStoredDecrease(double totalprice, string email)
        {
            try
            {
                double CurrentBalance = this.GetCreditStored(email);
                CurrentBalance -= totalprice;
                string queryString = "UPDATE accounts SET CreditStored =  " + CurrentBalance + " WHERE Email ='" + email + "'";
                MySqlCommand command = new MySqlCommand(queryString, connection);
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
            }
            catch (MySql.Data.MySqlClient.MySqlException)
            {
                MessageBox.Show("Error while loading...");
            }
        }

        public double GetCreditStored(string email)
        {
            double nr = 0;
            string sql = "SELECT CreditStored FROM accounts WHERE Email ='" + email + "'";
            MySqlDataReader reader = null;
            MySqlCommand command = new MySqlCommand(sql, connection);
            connection.Open();
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                nr = reader.GetDouble(0);
            }
            connection.Close();
            return nr;
        }

        public List<Items> GetItemsPrice()
        {
            List<Items> temp = new List<Items>();
            string sql = "SELECT * FROM items";
            MySqlCommand command = new MySqlCommand(sql, connection);
            try
            {
                connection.Open();
                Console.WriteLine("Got Connection");
                MySqlDataReader reader = command.ExecuteReader();
                Console.WriteLine("Started reader");
                int itemid;
                string itemname;
                double itemprice;
                while (reader.Read())
                {
                    Console.WriteLine("reader started");
                    itemid = Convert.ToInt32(reader["Id"]);
                    Console.WriteLine("Got Id");
                    itemname = Convert.ToString(reader["Name"]);
                    Console.WriteLine("Got Name");
                    itemprice = Convert.ToDouble(reader["PricePerHour"]);
                    Console.WriteLine("Got Price");
                    temp.Add(new Items(itemid, itemname, itemprice));
                }
            }
            catch
            {
                MessageBox.Show("Error while loading...");
            }
            finally
            {
                connection.Close();
            }
            return temp;
        }

        public List<Items> GetLoansInfo(ListBox list, string email)
        {
            List<Items> temp = new List<Items>();
            string sql = "SELECT * FROM items i join loans l on (i.Id = l.ItemId) WHERE l.AccountEmail = '" + email + "' AND IsReturned = 0";
            MySqlCommand command = new MySqlCommand(sql, connection);
            try
            {
                connection.Open();
                Console.WriteLine("Got Connection");
                MySqlDataReader reader = command.ExecuteReader();
                Console.WriteLine("Started reader");
                int itemid;
                string itemname;
                double itemprice;
                string accountemail;
                int isreturned;
                DateTime from;
                while (reader.Read())
                {
                    Console.WriteLine("reader started");
                    itemid = Convert.ToInt32(reader["Id"]);
                    Console.WriteLine("Got Id");
                    itemname = Convert.ToString(reader["Name"]);
                    Console.WriteLine("Got Name");
                    itemprice = Convert.ToDouble(reader["PricePerHour"]);
                    Console.WriteLine("Got Price");
                    accountemail = Convert.ToString(reader["AccountEmail"]);
                    Console.WriteLine("Got AccountEmail");
                    isreturned = Convert.ToInt32(reader["IsReturned"]);
                    Console.WriteLine("Got IsReturned");
                    from = Convert.ToDateTime(reader["From"]);
                    Console.WriteLine("Got From");
                    list.Items.Add(reader[0] + "." + reader[1] + " (" + reader[3] + " euros) From: " + reader[9] + ",IsReturned: " + reader[8]);
                }
            }
            catch
            {
                MessageBox.Show("Error while loading...");
            }
            finally
            {
                connection.Close();
            }
            return temp;
        }

        public List<Items> GetIsReturnedItems(string email)
        {
            List<Items> temp = new List<Items>();
            string sql = "SELECT * FROM loans l join items i on(l.ItemId = i.Id) WHERE AccountEmail = '" + email + "' AND IsReturned = 1";
            MySqlCommand command = new MySqlCommand(sql, connection);
            try
            {
                connection.Open();
                Console.WriteLine("Got Connection");
                MySqlDataReader reader = command.ExecuteReader();
                Console.WriteLine("Started reader");
                int itemid;
                string itemname;
                string accountemail;
                int isreturned;
                DateTime to;
                while (reader.Read())
                {
                    Console.WriteLine("reader started");
                    itemid = Convert.ToInt32(reader["Id"]);
                    Console.WriteLine("Got Id");
                    itemname = Convert.ToString(reader["Name"]);
                    Console.WriteLine("Got Name");
                    accountemail = Convert.ToString(reader["AccountEmail"]);
                    Console.WriteLine("Got AccountEmail");
                    isreturned = Convert.ToInt32(reader["IsReturned"]);
                    Console.WriteLine("Got IsReturned");
                    to = Convert.ToDateTime(reader["To"]);
                    Console.WriteLine("Got To");
                    temp.Add(new Items(itemid, itemname, to));
                }
            }
            catch
            {
                MessageBox.Show("Error while loading...");
            }
            finally
            {
                connection.Close();
            }
            return temp;
        }

        public void ReturnItemUpdateReturnTime(ListBox rented_list, string email)
        {
            try
            {
                foreach (string i in rented_list.Items)
                {
                    string item = i.ToString();
                    int id = Convert.ToInt32(item.ToString().Substring(0, 1));
                    string queryString = $"UPDATE `loans` SET `To` = '" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + "' WHERE `AccountEmail` = '" + email + "' AND `ItemId` = " + id + ";";
                    MySqlCommand command = new MySqlCommand(queryString, connection);
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
            }
            catch (MySql.Data.MySqlClient.MySqlException)
            {
                MessageBox.Show("Sorry,you can not return items without rfid card...");
            }
        }

        public void ReturnItemUpdateReturnStatus(ListBox rented_list, string email)
        {
            try
            {
                foreach (string i in rented_list.Items)
                {
                    string item = i.ToString();
                    int id = Convert.ToInt32(item.ToString().Substring(0, 1));
                    string queryString = $"UPDATE `loans` SET `IsReturned` = 1 WHERE `AccountEmail` = '" + email + "' AND `ItemId` = " + id + ";";
                    MySqlCommand command = new MySqlCommand(queryString, connection);
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
            }
            catch (MySql.Data.MySqlClient.MySqlException)
            {
                MessageBox.Show("Sorry,you can not return items without rfid card...");
            }
        }
    }
}
