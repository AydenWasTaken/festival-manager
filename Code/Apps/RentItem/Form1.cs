﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Phidget22;
using Phidget22.Events;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace RentItem
{
    public partial class Form1 : Form
    {
        private RFID MyRFIDReader;
        private List<Items> items = new List<Items>();
        DBRentItems dbrentitems;
        List<double> xx;

        public Form1()
        {
            InitializeComponent();
            dbrentitems = new DBRentItems();
            this.xx = new List<double>();
            this.StartUp();
            this.openrfid();
        }

        public void StartUp()
        {
            try
            {
                MyRFIDReader = new RFID();
                MyRFIDReader.Attach += new AttachEventHandler(ShowWhoIsAttached);
                MyRFIDReader.Detach += new DetachEventHandler(ShowWhoIsDetached);
                MyRFIDReader.Tag += new RFIDTagEventHandler(ProcessThisTag);
                MyRFIDReader.TagLost += rfid_TagLost;
                dbrentitems.GetItemsInfo(cbitemname);
            }
            catch (PhidgetException)
            {
                MessageBox.Show("Error at start up...");
            }
        }

        private void ShowWhoIsAttached(object sender,AttachEventArgs e)
        {
            lbrfidinfo.Items.Add("RFIDReader attached!, device serial nr: " + MyRFIDReader.DeviceSerialNumber);
        }

        private void ShowWhoIsDetached(object sender,DetachEventArgs e)
        {
            lbrfidinfo.Items.Add("RFIDReader detached!, device serial nr: " + MyRFIDReader.DeviceSerialNumber);
        }

        private void ProcessThisTag(object sender,RFIDTagEventArgs e)
        {
            lbrenteditem.Items.Clear();
            lbrfidinfo.Items.Add("rfid has tag-nr: " + e.Tag);
            try
            {
                if(SearchRFID(e.Tag) == null)
                {
                    MessageBox.Show("Account\nUnregistered");
                }
                else
                {
                    tbaccountid.Text = SearchRFID(e.Tag).AccountId;
                    tbaccountemail.Text = SearchRFID(e.Tag).Email;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Something wrong...");
            }
            if (tbaccountemail != null)
            {
                foreach (var i in this.dbrentitems.GetLoansInfo(lbrenteditem, tbaccountemail.Text))
                {
                    lbrenteditem.Items.Add(i);
                }
            }
        }

        private void rfid_TagLost(object sender,RFIDTagLostEventArgs e)
        {

        }

        private Customer SearchRFID(string id)
        {
            List<Customer> temp = dbrentitems.GetAccountsInfo();
            foreach (Customer c in temp)
            {
                if (c.GetAccountId() == id)
                {
                    return c;
                }
            }
            return null;
        }

        private void btrfidopen_Click(object sender, EventArgs e)
        {
        }
        private void openrfid()
        {
            try
            {
                MyRFIDReader.Open();
                lbrfid.Text = "RFID is already connected!";
            }
            catch (PhidgetException)
            {
                lbrfidinfo.Items.Add("No RFID opened...");
            }
        }

        private void cbitemname_SelectedIndexChanged(object sender, EventArgs e)
        {
            //double price = 0.00;
            //lbname.Items.Add(cbitemname.SelectedItem.ToString());
            //var selected = cbitemname.SelectedItem;
            //List<Items> temp = new List<Items>();
            //foreach (Items i in this.dbrentitems.GetItemsInfo(cbitemname))
            //{
            //    if()
            //    {
            //        price += i.Price;
            //    }
            //}
            //lbtotalprice.Text = Convert.ToString(price);
            //Items temp = (Items)lbname.SelectedItem;

            //double price = 0;
            string str = cbitemname.SelectedItem.ToString();
            lbname.Items.Add(str);
            this.CountTotalPrice();
            lbprice.Text = Convert.ToString(this.CountTotalPrice());
            //List<Items> temp = new List<Items>();
            //foreach (Items i in lbname.Items)
            //{
            //    Items items = (Items)i;
            //    temp.Add(items);
            //    price += i.Price;
            //    lbprice.Text = Convert.ToString(i.Price);
            //}
        }

        private double CountTotalPrice()
        {
            xx.Clear();
            foreach (string str in lbname.Items)
            {
                string[] sArray = str.Split(new char[3] { '.', ':', ' ' });
                double prc = Convert.ToDouble(sArray[2]);
                xx.Add(prc);
            }
            double sum = 0.00;
            foreach (double d in xx)
            {
                sum += d;
            }
            return sum;
        }

        private void btremove_Click(object sender, EventArgs e)
        {
            lbname.Items.Remove(lbname.SelectedItem.ToString());
            this.CountTotalPrice();
            lbprice.Text = Convert.ToString(this.CountTotalPrice());
        }

        private void btrent_Click(object sender, EventArgs e)
        {
            lbrenteditem.Items.Clear();
            if (this.dbrentitems.GetQuantiy(lbname) <= 0)
            {
                MessageBox.Show("Sorry,the stock quantity is not enough...");
            }
            else
            {
                if (this.dbrentitems.GetCreditStored(tbaccountemail.Text) <= 0)
                {
                    MessageBox.Show("Sorry,you have no money...");
                }
                else
                {
                    this.dbrentitems.QuantityDecrease(lbname);
                    this.dbrentitems.CreditStoredDecrease(this.CountTotalPrice(), tbaccountemail.Text);
                    this.dbrentitems.RentItem(lbname, tbaccountemail.Text);
                }
            }
            dbrentitems.GetLoansInfo(lbrenteditem, tbaccountemail.Text);
            lbname.Items.Clear();
        }

        private void btmoveup_Click(object sender, EventArgs e)
        {
            try
            {
                string temp = this.lbreturneditem.SelectedItem.ToString();
                lbrenteditem.Items.Add(temp);
                lbreturneditem.Items.Remove(temp);
            }
            catch (Exception)
            {
                MessageBox.Show("Something went wrong");
            }
        }

        private void btmovedown_Click(object sender, EventArgs e)
        {
            try
            {
                string temp = this.lbrenteditem.SelectedItem.ToString();
                lbreturneditem.Items.Add(temp);
                lbrenteditem.Items.Remove(temp);
            }
            catch (Exception)
            {
                MessageBox.Show("Something went wrong");
            }
        }

        private void btreturnitems_Click(object sender, EventArgs e)
        {
            this.dbrentitems.ReturnItemUpdateReturnTime(lbreturneditem, tbaccountemail.Text);
            this.dbrentitems.ReturnItemUpdateReturnStatus(lbreturneditem, tbaccountemail.Text);
            this.lbrenteditem.Items.Clear();
            this.dbrentitems.GetLoansInfo(lbrenteditem, tbaccountemail.Text);
            this.lbreturneditem.Items.Clear();
            lbshowreturnlist.Items.Clear();
            foreach (Items i in dbrentitems.GetIsReturnedItems(tbaccountemail.Text))
            {
                lbshowreturnlist.Items.Add("You has already returned item: " + i.ReturnedItemsInfo());
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            MyRFIDReader.Close();
            dbrentitems.connection.Close();
        }
    }
}
