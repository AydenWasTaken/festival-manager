﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentItem
{
    class Items
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public DateTime ToTime { get; set; }

        public Items(int id, string name, double price)
        {
            this.Id = id;
            this.Name = name;
            this.Price = price;
        }

        public Items(int id, string name, DateTime totime)
        {
            this.Id = id;
            this.Name = name;
            this.ToTime = totime;
        }

        public override string ToString()
        {
            return this.Name + " (" + this.Price + ")";
        }

        public string ReturnedItemsInfo()
        {
            return this.Id + "." + this.Name + " at " + this.ToTime;
        }
    }
}
