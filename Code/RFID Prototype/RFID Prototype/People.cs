﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RFID_Prototype
{
    class People
    {
        private List<Person> Dummies = new List<Person>();
        
        public bool AddPerson(Person p)
        {
            if (this.SearchPerson(p.getId()) == null)
            {
                this.Dummies.Add(p);
                return true;
            }
            else
            {
                return false;
            }
        }
        public Person SearchPerson(string id)
        {
            foreach (Person p in this.Dummies)
            {
                if (p.getId() == id)
                { return p; }
            }
            return null;
        }
        public List<Person> SearchPeople(string name)
        {
            List<Person> temp = new List<Person>();
            foreach (Person p in this.Dummies)
            {
                if (p.Name == name)
                {
                    temp.Add(p);
                }
            }
            return temp;
        }
        public List<Person> ShowAllPeople()
        {
            return this.Dummies;
        }

    }
}
