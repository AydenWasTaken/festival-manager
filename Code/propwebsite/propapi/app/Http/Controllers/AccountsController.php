<?php

namespace App\Http\Controllers;

use App\account;
use App\Http\Resources\Account as AccountResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use DB;
//use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;

class AccountsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get accounts
        //$accounts = account::paginate(15);
        $accounts = account::all();
        return AccountResource::collection($accounts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $account = new account();
        $data = Input::all(); //this is an associative array with the correct data

        /* todo: Ask teacher. How do i send mail without authentication?
         * Error: Expected response code 250 but got code \"530\", with message \"530 5.7.1 Authentication required\r\n\"", exception: "Swift_TransportException
         *
         * steps to reproduce:
         * 1. register on website with specified code in this method
         *
         * failing code:
         * Mail::to('nils.moller@live.nl')->send(new Registered($data['Name']));
         */

        $account->Email = $data['Email'];
        $account->Name = $data['Name'];
        $account->Password = $data['Password'];
        $account->Gender = $data['Gender']; //is 0/1 in the db
        $account->Phone = $data['Phone'];
        $account->CheckedInEvent = $data['CheckedInEvent'];
        $account->CheckedInCamping = $data['CheckedInCamping'];
        $account->Address = $data['Address'];
        $account->DateOfBirth = $data['DateOfBirth']; //1997-07-28 00:00:00 format
        $account->RFID = $data['RFID'];
        $account->TicketId = $data['TicketId'];
        $account->PaidForTicket = $data['PaidForTicket'];
        $account->CreditStored = 0;
        $account->RFID = '2800b8532e'; //todo: temporary, for the presentation
        $account->save();



        return new AccountResource($account);
    }

    /**
     * return account array where email and password are in same row
     * empty array if none found
     *
     * @param $email
     * @param $password
     * @return mixed
     */
    public function showByEmailPassword($email, $password){
        //return account::where('Email', 'LIKE', $email)->and('Password', 'LIKE', $password)->get();

        return account::where([
            ['Email', 'LIKE', $email],
            ['Password', 'LIKE', $password]])->get();
    }

    /**
     * same as showByEmailPassword but only for email
     *
     * @param $email
     * @return mixed
     */
    public function showByEmail($email){
        //return account::where('Email', 'LIKE', $email)->and('Password', 'LIKE', $password)->get();

        return account::where('Email', 'LIKE', $email)->get();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request  $request
     * @param $email
     * @return \Illuminate\Http\Response
     */
    public function updatePhone(Request $request, $email)
    {
        $data = Input::all(); //this is an associative array with the correct data
        $accounts = $this->showByEmail($email);
        $account = $accounts[0];

        DB::table('accounts')
            ->where('Email', $email)
            ->update(['Phone' => $data['Phone']]);

        return new AccountResource($account);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request  $request
     * @param $email
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(Request $request, $email) //request is inputted data
    {
        $data = Input::all(); //this is an associative array with the correct data
        $accounts = $this->showByEmail($email);
        $account = $accounts[0];

        DB::table('accounts')
            ->where('Email', $email)
            ->update(['Password' => $data['Password']]);

        return new AccountResource($account);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request  $request
     * @param $email
     * @return \Illuminate\Http\Response
     */
    public function addCredit(Request $request, $email) //request is inputted data
    {
        $data = Input::all(); //this is an associative array with the correct data
        $accounts = $this->showByEmail($email);
        $account = $accounts[0];

        DB::table('accounts')
            ->where('Email', $email)
            ->update(['CreditStored' => $data['CreditStored']]);

        return new AccountResource($account);
    }

    /**
     * find by email and destroy said account
     * return the account if destroy was succesfull
     *
     * @param $email
     * @return AccountResource
     */
    public function destroy($email)
    {
        $account = $this->showByEmail($email);
        DB::table('accounts')->where('email', '=', $email)->delete();
        return new AccountResource($account);
    }
}
