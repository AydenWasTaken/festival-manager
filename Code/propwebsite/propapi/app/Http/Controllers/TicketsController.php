<?php

namespace App\Http\Controllers;

use App\Http\Resources\TicketResource;
use App\ticket;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
//use Illuminate\Support\Facades\Log;
use DateTime;

class TicketsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get all tickets, 15 per page
        //$tickets = ticket::paginate(15);
        $tickets = ticket::all();
        return TicketResource::collection($tickets);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ticket = new ticket();
        $data = Input::all(); //this is an associative array with the correct data
        $accountdata = $data[0]; //session account
        $ticketdata = $data[1]; //passed ticket

        $ticket->DateOfPurchase = new DateTime('now');
        $ticket->AmountOfPeople = $ticketdata['AmountOfPeople'];
        $ticket->Price = $ticketdata['Price'];

        if (key_exists('CampingId', $ticketdata)){
            $ticket->CampingId = $ticketdata['CampingId'];
            DB::table('campings')
                ->where('Id', $ticketdata['CampingId'])
                ->update(['isAvailable' => 0]);
        } //if they want to camp

        $ticket->save();

        $maxticketid = ticket::max('Id'); //max ticket id, should be last created ticket
        if (key_exists('OtherEmails', $ticketdata)){
            foreach ($ticketdata['OtherEmails'] as $email){
                DB::table('accounts')
                    ->where('Email', $email)
                    ->update(['TicketId' => $maxticketid]);
            }
        }


        DB::table('accounts')
            ->where('Email', $accountdata['Email'])
            ->update(['TicketId' => $maxticketid]);

        //Mail::to($request->user())->send(new BoughtTicket()); //todo: mail ticket bought. same deal as registration

        return new TicketResource($ticket);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//        $ticket = ticket::findOrFail($id);
//        return new TicketResource($ticket);
        return ticket::where('Id', '=', $id)->get();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ticket = ticket::findOrFail($id); //it does get the ticket
        DB::table('tickets')->where('Id', '=', $id)->delete();
        return new TicketResource($ticket); //returns the right ticket
    }
}
