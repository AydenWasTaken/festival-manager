<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ticket extends Model
{
    public $timestamps = false;
    protected $table = 'tickets';

    protected $fillable = [
        'DateOfPurchase',
        'CampingId',
        'AmountOfPeople',
        'Price',
        'OtherEmails'
    ];
}
