import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Account} from './classes/account';
import {Ticket} from './classes/ticket';
import {Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class ApiInteractionService {
    
    private URLgeneral: string = 'http://local.propapi.com/api/';
    
    constructor(private http: HttpClient) {
    }
    
    public getAccount(Email: string, Password: string): Observable<Account> {
        return this.http.get<Account[]>(this.URLgeneral + 'account/' + Email + '/' + Password).pipe(
            map(a => {
                if (a[0] == null){
                    console.log('(getAccount() - api service) got no data');
                    return null; // mapping resultant observable value to null
                }
                else {
                    console.log('(getAccount() - api service) account found');
                    return a[0]; //mapping the resultant observable return value to the first array element
                }
            })
        );
    }
    
    public getAllAccounts(): Observable<Account[]>{
        return this.http.get<Account[]>(this.URLgeneral + 'accounts');
    }
    
    public getAccountByEmail(Email): Observable<Account>{
        return this.http.get<Account[]>(this.URLgeneral + 'account/' + Email).pipe(
            map(a => {
                if (a[0] == null){
                    console.log('(getAccountByEmail() - api service) got no data');
                    return null; // mapping resultant observable value to null
                }
                else {
                    console.log('(getAccountByEmail() - api service) account found');
                    return a[0]; //mapping the resultant observable return value to the first array element
                }
            })
        );
    }
    
    public postAccount(a: Account): Observable<Account>{
        return this.http.post<Account>(this.URLgeneral + 'newAccount', a).pipe(
            tap (() => console.log('(postAccount() - api service) posted account with email: ' + a.Email))
        );
    }
    
    public updateAccountPhone(a: Account): Observable<Account>{
        return this.http.put<Account>(this.URLgeneral + 'updatePhone/' + a.Email, a).pipe(
            tap (() => console.log('(updateAccountPhone() - api service) updated phone for account with email: ' + a.Email))
        );
    }
    
    public updateAccountPassword(a: Account): Observable<Account>{
        return this.http.put<Account>(this.URLgeneral + 'updatePassword/' + a.Email, a).pipe(
            tap (() => console.log('(updateAccountPassword() - api service) updated password for account with email: ' + a.Email))
        );
    }

    public addCredit(a: Account): Observable<Account>{
        return this.http.put<Account>(this.URLgeneral + 'addCredit/' + a.Email, a).pipe(
            tap (() => console.log('(addCredit() - api service) added credit for account with email: ' + a.Email))
        );
    }
    
    
    public getTicket(id: number): Observable<Ticket> { //use passed account to get ticket from ticket id in account
        return this.http.get<Ticket[]>(this.URLgeneral + 'ticket/' + id).pipe(
            map(t => {
                if (t['data'] == null){
                    console.log('(getTicket() - api service) got no data');
                    return null; // mapping resultant observable value to null
                }
                else {
                    console.log('(getTicket() - api service) ticket found');
                    return t['data']; //mapping the resultant observable return value to the first array element
                }
            })
        );
    }
    
    public postTicket(t: Ticket): Observable<Ticket>{
        
        let a: Account = JSON.parse(sessionStorage.getItem('account'));
        return this.http.post<Ticket>(this.URLgeneral + 'newticket', [a, t]).pipe(
            tap(() => {
                console.log('(postTicket() - api service) posted ticket');
            })
        );
        
    }
    
    public getFirstFreeSpot(): Observable<number>{
        return this.http.get<number>(this.URLgeneral + 'freespot').pipe(
            tap ((spot) => {
                if (spot)
                    console.log('(getFirstFreeSpot() - api service) found free spot');
                else
                    console.log('(getFirstFreeSpot() - api service) no free spot found');
            })
        );
    }
}
