import {Injectable} from '@angular/core';
import {Account} from './classes/account';
import {Ticket} from './classes/ticket';
import {ApiInteractionService} from './api-interaction.service';

@Injectable({
    providedIn: 'root'
})

export class BuyticketService {
    sessionAccount: Account = JSON.parse(sessionStorage.getItem('account'));
    temp: any;
    constructor(private api: ApiInteractionService) { }

    getTotalPrice(isCamping: boolean, amountoftickets: number): number{ //returning 0
        let temp2: any;
        if (isCamping == true){
            switch (amountoftickets) {
                case 1: {
                    temp2 = (55) + 20 + (10);
                    break;
                }
                case 2: {
                    temp2 = (55 * 2)+ 20 + (10 * 2);
                    break;
                }
                case 3: {
                    temp2 = (55 * 3)+ 20 + (10 * 3);
                    break;
                }
                case 4: {
                    temp2 = (55 * 4)+ 20 + (10 * 4);
                    temp2 = (temp2 / 100 * 95);
                    break;
                }
                case 5: {
                    temp2 = (55 * 5)+ 20 + (10 * 5);
                    temp2 = (temp2 / 100 * 90);
                    break;
                }
                case 6: {
                    temp2 = (55 * 6)+ 20 + (10 * 6);
                    temp2 = (temp2 / 100 * 85);
                    break;
                }
            }
        }
        else if (isCamping == false){
            switch (amountoftickets) {
                case 1: {
                    temp2 = 55;
                    break;
                }
                case 2: {
                    temp2 = 55 * 2;
                    break;
                }
                case 3: {
                    temp2 = 55 * 3;
                    break;
                }
                case 4: {
                    temp2 = 55 * 4;
                    temp2 = (temp2 / 100) * 95;
                    break;
                }
                case 5: {
                    temp2 = 55 * 5;
                    temp2 = (temp2 / 100) * 90;
                    break;
                }
                case 6: {
                    temp2 = 55 * 6;
                    temp2 = (temp2 / 100) * 85;
                    break;
                }
            }
        }
        temp2 = temp2.toFixed(2);
        return temp2;
    }

    buyTicket(amountOfPeople: number, isCamping: boolean, price: number, otheremails: string[]): Ticket{
        /*region checks*/
        if (this.sessionAccount == null){
            alert('Please log in/register before buying a ticket');
            console.log('(buyTicket() - buyTicketService) sessionAccount was null');
            return;
        } //check if logged in
        else if (this.sessionAccount.TicketId != null){
            alert('You already have a ticket.');
            console.log('(buyTicket() - buyTicketService) sessionAccount already has a ticket');
            return;
        } //checked for already existing ticket
        else if (!confirm('Are you sure?')){
            console.log('(buyTicket() - buyTicketService) user chose cancel');
            return;
        } //check for customer certainty
        else if (this.sessionAccount.CreditStored < price){
            console.log('(buyTicket() - buyTicketService)not enough credit');
            alert('Not enough credit in your account');
            return;
        }
        /*endregion*/

        this.api.getAllAccounts().subscribe(response => {
            let allemails: string[] = [];
            /*region check emails for existance*/
            for (let a of response['data']){
                allemails.push(a.Email);
            } //for every account in the db, add their email to allemails

            const filtered = allemails.filter(x => otheremails.includes(x)); //checks if other emails exists in database

            if (filtered.length != otheremails.length){ //if they are not the same not all emails are in db
                alert('Not all emails were found. Make sure everyone has an account.');
                return;
            }
            /*endregion*/

            if (isCamping == true){
                this.api.getFirstFreeSpot().subscribe(spot => {
                    if (spot != null){ //check if spot exists
                        this.temp = new Ticket(amountOfPeople, parseInt(price.toString()), spot, otheremails); //create new ticket
                        this.api.postTicket(this.temp).subscribe(
                            () => {
                                sessionStorage.setItem('ticket', JSON.stringify(this.temp)); //set session ticket to found ticket
                                this.api.getAccountByEmail(this.sessionAccount.Email).subscribe(acc => {
                                    sessionStorage.setItem('account', JSON.stringify(acc));
                                    this.sessionAccount = acc;
                                });
                                alert('Done!');
                                location.reload();
                                return this.temp;
                            }
                        ); //post to api
                    }
                    else {
                        alert('No camping spots left. Terminating');
                        return;
                    }
                });
            } //if camping
            else {
                this.temp = new Ticket(amountOfPeople, parseInt(price.toString()), null, otheremails);
                this.api.postTicket(this.temp).subscribe(
                    () => {
                        sessionStorage.setItem('ticket', JSON.stringify(this.temp)); //set session ticket to found ticket
                        this.api.getAccountByEmail(this.sessionAccount.Email).subscribe(acc => {
                            sessionStorage.setItem('account', JSON.stringify(acc));
                            this.sessionAccount = acc;
                        });
                        alert('Done!');
                        location.reload();
                        return this.temp;
                    }
                ); //post to api
            } //if not camping
        });
    }
}
