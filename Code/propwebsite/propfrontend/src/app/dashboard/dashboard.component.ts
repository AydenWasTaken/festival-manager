import { Component, OnInit } from '@angular/core';
import {AppComponent} from '../app.component';
import {Account} from '../classes/account';
import {Ticket} from '../classes/ticket';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  sessionAccount: Account = JSON.parse(sessionStorage.getItem('account'));
  sessionTicket: Ticket = JSON.parse(sessionStorage.getItem('ticket'));
  
  constructor(public AppComponent: AppComponent) { }

  ngOnInit() {
  }

}
