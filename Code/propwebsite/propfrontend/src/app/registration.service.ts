import {Injectable} from '@angular/core';
import {Account} from './classes/account';
import {ApiInteractionService} from './api-interaction.service';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class RegistrationService {

    constructor(private api: ApiInteractionService) {
    }
    
    public register(Name, Password, RepeatedPassword, Email, Gender, Phone, Address, DateOfBirth): Observable<Account> {
        
        if (Name == null || Email == null || Password == null || RepeatedPassword == null || DateOfBirth == null || Gender == -1 || Address == null || Phone == null){ //check for empty fields
            window.alert("Please fill in all fields");
            return;
        }
        else if (Password != RepeatedPassword){ //check for password match
            window.alert("Passwords don't match.");
            return;
        }
        this.api.getAccountByEmail(Email).subscribe(a => {
                if (a != null){
                    window.alert('Already have an account with that email');
                    return;
                }
            });
        console.log('(register(...) - registrationService) - checks complete'); //log
        let a: Account = new Account(Email, Name, Password, Gender, Phone, Address, DateOfBirth);
        return this.api.postAccount(a).pipe( //Name, Password, Email, Gender, Phone, Address, DateOfBirth, CheckedInEvent, CreditStored, CheckedInCamping, RFID, TicketId, PaidForTicket
            tap(() => {
                console.log('(register(...) - registrationService) - posted account to api'); //
                sessionStorage.setItem('account', JSON.stringify(a)); //set session variable with key 'account' (as reference) with the passed account as data
                window.alert('Logged in as ' + a.Name + '.'); //show popup window
                location.replace('/landing'); //go back to home-page with a reload so the login shows up
            })
        );
    }
}
