import {Component, OnInit} from '@angular/core';
import {RegistrationService} from '../registration.service';
import {Account} from '../classes/account';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

    // name: string = 'm';
    // email: string = 'ha.ha@live.nl';
    // password: string = '123456';
    // repeatedpassword: string = '123456';
    // DateOfBirth: string = '2000-05-02';
    // gender: string = 'male';
    // address: string = 'Heezerweg 117, Eindhoven';
    // phone: string = '0642868029';

    name: string;
    email: string;
    password: string;
    repeatedpassword: string;
    DateOfBirth: string;
    gender: number = -1;
    address: string;
    phone: string;
    
    subscription: Subscription;

    constructor(private RegistrationService: RegistrationService) { }

    ngOnInit() {
    }

    public register() {
        this.subscription = this.RegistrationService.register(this.name, this.password, this.repeatedpassword, this.email, this.gender, this.phone, this.address, this.DateOfBirth).subscribe();
    }
    
    ngDestroy(){
        if (this.subscription)
            this.subscription.unsubscribe();
    }
}
