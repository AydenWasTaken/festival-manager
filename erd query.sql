CREATE TABLE propSTALLS(
             StallID           INT   NOT NULL,
             EventID           INT   NOT NULL,
             EmployeeID        INT   NOT NULL,
             CONSTRAINT   STALLSPK   PRIMARY KEY(StallID)
                   );

CREATE TABLE propEVENT(
             EventName           CHAR(50)   NOT NULL,
             EmployeeCount       INT        NOT NULL,
             CustomerCount       INT        NOT NULL,
             StallCount          INT        NOT NULL,
             EventID             INT        NOT NULL,
             EventStatus         CHAR(25)   NOT NULL,
             EventStartDate      CHAR(25)   NOT NULL,
             EventEndDate        CHAR(25)   NOT NULL,
             CONSTRAINT   EVENTPK   PRIMARY KEY(EventID)
                 );

CREATE TABLE propEVENT_LINEUP(
             EventID    INT   NOT NULL,
             BandID     INT   NOT NULL,
             CONSTRAINT   EVENT_LINEUPPK   PRIMARY KEY(EventID)
                         );

CREATE TABLE propLINEUPS(
             Band           CHAR(50)   NOT NULL,
             Description    CHAR(50)   NULL,
             Time           CHAR(25)   NULL,
             Genre          CHAR(50)   NULL,
             BandID         INT        NOT NULL,
             EventId        INT        NOT NULL,
             CONSTRAINT   LINEUPSPK   PRIMARY KEY(BandID)           
                  );

CREATE TABLE propITEMS_STALLS(
             StallID         INT    NOT NULL,
             ItemID          INT    NOT NULL,
             IsLoaningItem   BOOL   NOT NULL,
             CONSTRAINT   ITEMS_STALLSPK   PRIMARY KEY(StallID)
                         );

CREATE TABLE propDISCOUNTS(
             TicketType       CHAR(50)        NOT NULL,
             DiscountPercent  DECIMAL         NOT NULL,
             CONSTRAINT   DISCOUNTSPK   PRIMARY KEY(TicketType)
                         );
                         
CREATE TABLE propTICKETS(
             AccountID         INT        NOT NULL,
             TimeOfPurchase    CHAR(25)   NULL,
             isCamping         BOOL       NOT NULL,
             EventID           INT        NOT NULL,
             RFIDKey           INT        NOT NULL,
             TicketType        CHAR(50)   NOT NULL,
             CONSTRAINT   TICKETSPK   PRIMARY KEY(AccountID)
                    );

CREATE TABLE propRFIDS(
             RFIDKey       INT   NOT NULL,
             AccountID     INT   NOT NULL,
             EventID       INT   NOT NULL,
             CONSTRAINT   RFIDSPK   PRIMARY KEY(RFIDKey)
                  );

CREATE TABLE propCAMPSITES(
             SpotNo           INT        NOT NULL,
             isAvailable      CHAR(25)   NOT NULL,
             EventID          INT        NOT NULL,
             CONSTRAINT   CAMPSITESPK   PRIMARY KEY(SpotNo)
                      );

CREATE TABLE propEMPLOYEES(
             EmployeeID      INT         NOT NULL,
             LastName        CHAR(25)    NOT NULL,
             FirstName       CHAR(25)    NOT NULL,
             ManagerID       INT         NOT NULL,
             Email           CHAR(50)    NULL,
             Password        CHAR(50)    NOT NULL,
             DepartmentID    INT         NULL,
             EventID         INT         NOT NULL,
             CONSTRAINT   EMPLOYEESPK   PRIMARY KEY(EmployeeID)
                      );                      

CREATE TABLE propDEPARTMENTS(
             DepartmentID     INT        NOT NULL,
             DepartmentName   CHAR(25)   NOT NULL,
             CONSTRAINT   DEPARTMENTSPK   PRIMARY KEY(DepartmentID)
                        );

CREATE TABLE propWITHDRAWELS(
             WithDrawelID         INT        NOT NULL,         
             AccountID            INT        NOT NULL,
             WithDrawelAmount     INT        NOT NULL,
             Status               CHAR(25)   NOT NULL,
             CONSTRAINT   WITHDRAWELSPK   PRIMARY KEY(WithDrawelID)
                        );

CREATE TABLE propACCOUNTS(
             AccountID          INT        NOT NULL,
             LastName           CHAR(25)   NOT NULL,
             FirstName          CHAR(25)   NOT NULL,
             Password           CHAR(50)   NOT NULL,
             Email              CHAR(50)   NULL,
             Gender             CHAR(25)   NULL,
             Phone              CHAR(25)   NULL,
             CheckedInEvent     CHAR(25)   NOT NULL,
             TicketID           INT        NOT NULL,
             CreditStored       CHAR(25)   NOT NULL,
             CheckedInCamping   CHAR(25)   NOT NULL,
             Address            CHAR(50)   NULL,
             Age                INT        NULL, 
             EventID            INT        NOT NULL,
             CONSTRAINT   ACCOUNTSPK   PRIMARY KEY(AccountID)
                   );                  

CREATE TABLE propDEPOSITS(
             TransferID          INT        NOT NULL,
             AccountID           INT        NOT NULL,
             DepositAmount       INT        NOT NULL,
             Status              CHAR(25)   NOT NULL,
             CONSTRAINT   DEPOSITSPK   PRIMARY KEY(TransferID)
                     );
                   
CREATE TABLE propLOANS(
             AccountID    INT        NOT NULL,
             ItemID       INT        NOT NULL,
             StartTime    CHAR(25)   NOT NULL,
             EndTime      CHAR(25)   NOT NULL,
             CONSTRAINT   LOANSPK   PRIMARY KEY(AccountID)
                );

CREATE TABLE propPURCHASES(
             AccountID        INT          NOT NULL,
             ItemID           INT          NOT NULL,
             TimeOfPurchase   CHAR(25)     NOT NULL,
             CONSTRAINT   PURCHASESPK   PRIMARY KEY(AccountID)
                      );

CREATE TABLE propLOANING_ITEMS(
             ItemID      INT        NOT NULL,
             Name        CHAR(25)   NOT NULL,
             Price       INT        NOT NULL,
             TotalSold   INT        NOT NULL,
             CONSTRAINT   LOANING_ITEMSPK   PRIMARY KEY(ItemID)
                 );

CREATE TABLE propPURCHASE_ITEMS(
             ItemID      INT        NOT NULL,
             Name        CHAR(25)   NOT NULL,
             Price       INT        NOT NULL,
             TotalSold   INT        NOT NULL,
             CONSTRAINT   PURCHASE_ITEMSPK   PRIMARY KEY(ItemID)
                           );
                           
                           
ALTER TABLE propSTALLS ADD CONSTRAINT   ITEMS_STALLSFK   FOREIGN KEY(StallID) REFERENCES propITEMS_STALLSFK(StallID);
ALTER TABLE propSTALLS ADD CONSTRAINT   EVENTFK  FOREIGN KEY(EventID) REFERENCES propEVENT(EventID);
ALTER TABLE propSTALLS ADD CONSTRAINT   EMPLOYEESFK   FOREIGN KEY(EventId) REFERENCES propEMPLOYEES(EventID);

ALTER TABLE propEVENT ADD CONSTRAINT   STALLSFK   FOREIGN KEY(EventID)   REFERENCES   propSTALLS(StallID);
ALTER TABLE propEVENT ADD CONSTRAINT   EMPLOYEESFK   FOREIGN KEY(EventID)   REFERENCES   propEMPLOYEES(EmployeeID);
ALTER TABLE propEVENT ADD CONSTRAINT   TICKETSFK   FOREIGN KEY(EventID)   REFERENCES   propTICKETS(AccountID);
ALTER TABLE propEVENT ADD CONSTRAINT   CAMPSITESFK   FOREIGN KEY(EventID)   REFERENCES   propCAMPSITES(SportNo);
ALTER TABLE propEVENT ADD CONSTRAINT   EVENT_LINEUPFK   FOREIGN KEY(EventID)   REFERENCES   propEVENT_LINEUP(EventID);
ALTER TABLE propEVENT ADD CONSTRAINT   ACCOUNTSFK   FOREIGN KEY(EventID)   REFERENCES   propACCOUNTS(EventID);

ALTER TABLE propEVENT_LINEUP ADD CONSTRAINT   EVENTFK   FOREIGN KEY(EventID)   REFERENCES   propEVENT(EventID);
ALTER TABLE propEVENT_LINEUP ADD CONSTRAINT   LINEUPSFK   FOREIGN KEY(BandID)   REFERENCES   propLINEUP(BandID);

ALTER TABLE propLINEUPS ADD CONSTRAINT   EVENT_LINEUPFK   FOREIGN KEY(BandID)   REFERENCES   propEVENT_LINEUP(EventID);
                         
ALTER TABLE propITEMS_STALLS ADD CONSTRAINT   STALLSFK   FOREIGN KEY(StallID)   REFERENCES propSTALLS(StallID);
ALTER TABLE propITEMS_STALLS ADD CONSTRAINT   LOANING_ITEMSFK   FOREIGN KEY(ItemID) REFERENCES propLOANING_ITEMS(ItemID);
ALTER TABLE propITEMS_STALLS ADD CONSTRAINT   PURCHASE_ITEMSFK   FOREIGN KEY(ItemID) REFERENCES propPURCHASE_ITEMS(ItemID);

ALTER TABLE propDISCOUNTS ADD CONSTRAINT   TICKETSFK   FOREIGN KEY(TicketType)   REFERENCES   propTICKETS(AccountID);

ALTER TABLE propTICKETS ADD CONSTRAINT   EVENTFK   FOREIGN KEY(EventID)   REFERENCES   propEVENT(EventID);
ALTER TABLE propTICKETS ADD CONSTRAINT   ACCOUNTSFK   FOREIGN KEY(AccountID)   REFERENCES   propACCOUNTS(AccountID);
ALTER TABLE propTICKETS ADD CONSTRAINT   AccountIDFK   FOREIGN KEY(AccountID)   REFERENCES   propAccountID(TicketNo);
ALTER TABLE propTICKETS ADD CONSTRAINT   DISCOUNTSFK   FOREIGN KEY(TicketType)   REFERENCES   propDISCOUNTS(TicketType);
ALTER TABLE propTICKETS ADD CONSTRAINT   RFIDSFK   FOREIGN KEY(RFIDKey)   REFERENCES   propRFIDS(RFIDKey);

ALTER TABLE propRFIDS ADD CONSTRAINT   TICKETSFK   FOREIGN KEY(RFIDKey)   REFERENCES propTICKETS(AccountID);

ALTER TABLE propCAMPSITES ADD CONSTRAINT   AccountIDFK   FOREIGN KEY(SpotNo)   REFERENCES propAccountID(TicketNo);
ALTER TABLE propCAMPSITES ADD CONSTRAINT   EVENTFK   FOREIGN KEY(EventID)   REFERENCES   propEVENT(EventID);

ALTER TABLE propEMPLOYEES ADD CONSTRAINT   DEPARTMENTSFK   FOREIGN KEY(DepartmentID)   REFERENCES propDEPARTMENTS(DepartmentID);
ALTER TABLE propEMPLOYEES ADD CONSTRAINT   STALLSFK   FOREIGN KEY(EventID)   REFERENCES   propSTALLS(StallID);
ALTER TABLE propEMPLOYEES ADD CONSTRAINT   EVENTFK   FOREIGN KEY(EventID)   REFERENCES   propEVENT(EventID);

ALTER TABLE propDEPARTMENTS ADD CONSTRAINT   EMPLOYEESFK   FOREIGN KEY(DepartmentID)   REFERENCES   propEMPLOYEES(EmployeeID);

ALTER TABLE propWITHDRWAELS ADD CONSTRAINT   ACCOUNTSFK   FOREIGN KEY(AccountID)   REFERENCES   propACCOUNTS(AccountID);

ALTER TABLE propACCOUNTS ADD CONSTRAINT   EVENTFK   FOREIGN KEY(AccountID)   REFERENCES propEVENT(EventID);
ALTER TABLE propACCOUNTS ADD CONSTRAINT   TICKETSFK   FOREIGN KEY(AccountID)   REFERENCES   propTICKETS(AccountID);
ALTER TABLE propACCOUNTS ADD CONSTRAINT   LOANSFK   FOREIGN KEY(AccountID)   REFERENCES   propLOANS(AccountID);
ALTER TABLE propACCOUNTS ADD CONSTRAINT   PURCHASES   FOREIGN KEY(AccountID)   REFERENCES   propPURCHASES(AccountID);
ALTER TABLE propACCOUNTS ADD CONSTRAINT   WITHDRAWELSFK   FOREIGN KEY(AccountID)   REFERENCES   propWITHDRAWELS(WithDrawelID);
ALTER TABLE propACCOUNTS ADD CONSTRAINT   DEPOSITSFK   FOREIGN KEY(AccountID)   REFERENCES   propDEPOSITS(TransferID);

ALTER TABLE propDEPOSITS ADD CONSTRAINT   ACCOUNTSFK   FOREIGN KEY(AccountID)   REFERENCES   propACCOUNTS(AccountID);

ALTER TABLE propLOANS ADD CONSTRAINT   ACCOUNTSFK   FOREIGN KEY(AccountID)   REFERENCES   propACCOUNTS(AccountID);
ALTER TABLE propLOANS ADD CONSTRAINT   LOANING_ITEMSFK   FOREIGN KEY(ItemID)   REFERENCES   propLOANING_ITEMS(ItemID);

ALTER TABLE propPURCHASES ADD CONSTRAINT   ACCOUNTSFK   FOREIGN KEY(AccountID)   REFERENCES   propACCOUNTS(AccountID);
ALTER TABLE propPURCHASES ADD CONSTRAINT   PURCHASE_ITEMSFK   FOREIGN KEY(ItemID)   REFERENCES   propPURCHASE_ITEMS(ItemID);

ALTER TABLE propLOANING_ITEMS ADD CONSTRAINT   LOANSFK   FOREIGN KEY(ItemID)   REFERENCES   propLOANS(AccountID);
ALTER TABLE propLOANING_ITEMS ADD CONSTRAINT   ITEMS_STALLSFK   FOREIGN KEY(ItemID)   REFERENCES propITEMS_STALLS(StallID);

ALTER TABLE propPURCHASE_ITEMS ADD CONSTRAINT   PURCHASESFK   FOREIGN KEY(ItemID)   REFERENCES   propPURCHASES(AccountID);
ALTER TABLE propPURCHASE_ITEMS ADD CONSTRAINT   ITEMS_STALLSFK   FOREIGN KEY(ItemID)   REFERENCES propITEMS_STALLS(StallID);